import app from './app'
import initializeDatabase from './db'
  
  const start = async () => {
    const controller = await initializeDatabase()
    
    app.get('/', (req, res, next) => res.send("ok"));
  
  
    // CREATE
    app.get('/laces/new', async (req, res, next) => {
      const { lace_color, lace_2nd_color, on_shoe } = req.query
      const result = await controller.createContact({lace_color,lace_2nd_color, on_shoe})
      res.json({success:true, result})
    })
  
    // READ
    app.get('/laces/get/:id', async (req, res, next) => {
      const { id } = req.params
      const lace = await controller.getLace(id)
      res.json({success:true, result:lace})
    })
  
    // DELETE
    app.get('/laces/delete/:id', async (req, res, next) => {
      const { id } = req.params
      const result = await controller.deleteLace(id)
      res.json({success:true, result})
    })
  
    // UPDATE
    app.get('/laces/update/:id', async (req, res, next) => {
      const { id } = req.params
      const { lace_color, lace_2nd_color, on_shoe } = req.query
      const result = await controller.updateContact(id,{lace_color,lace_2nd_color, on_shoe})
      res.json({success:true, result})
    })
  
    // LIST
    app.get('/laces/list', async (req, res, next) => {
      const { order } = req.query
      const laces = await controller.getLacesList(order)
      res.json({success:true, result:laces})
    })
  }

  start()
  app.use((err, req, res, next) => {
    try {
    console.error(err)
    const message = err.message
    res.status(500).json({ success:false, message })
    } catch(e){
      next(e)
    }  
  })

  app.listen(8080, () => console.log('server listening on port 8080'))




