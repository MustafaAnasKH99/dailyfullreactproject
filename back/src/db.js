import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  const createLace = async (props) => {
    if(!props || !props.lace_color || !props.lace_2nd_color){
      throw new Error(`you must provide any lace color`)
    }
    const { lace_color, lace_2nd_color, on_shoe } = props
    try{
    const result = await db.run(SQL`INSERT INTO laces (lace_color,lace_2nd_color, on_shoe) VALUES (${lace_color}, ${lace_2nd_color}), ${on_shoe})`);
    const id = result.stmt.lastID
    return id
    } catch(e) {
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }
  
  const deleteLace = async (lace_id) => {
    try {
    const result = await db.run(SQL`DELETE FROM laces WHERE lace_id = ${lace_id}`);
    if(result.stmt.changes === 0){
      throw new Error(`lace "${id}" does not exist`)
    }
    return true
  }catch(e){
    throw new Error(`couldn't delete the lace "${id}": `+e.message)
  }
}

  
  const updateLace = async (id, props) => {
    if (!props || !(props.lace_color || props.lace_2nd_color)) {
      throw new Error(`you must provide any first or second lace color`);
    }
    const { lace_color, lace_2nd_color } = props;
    try {
      let statement = "";
      if (lace_color && lace_2nd_color) {
        statement = SQL`UPDATE contacts SET lace_2nd_color=${lace_2nd_color}, lace_color=${lace_color}, on_shoe=${on_shoe} WHERE lace_id = ${id}`;
      }
      else if (lace_color && lace_2nd_color) {
        statement = SQL`UPDATE contacts SET lace_2nd_color=${lace_2nd_color}, lace_color=${lace_color} WHERE lace_id = ${id}`;
      } else if (lace_color) {
        statement = SQL`UPDATE contacts SET lace_color=${lace_color} WHERE lace_id = ${id}`;
      } else if (lace_2nd_color) {
        statement = SQL`UPDATE contacts SET lace_2nd_color=${lace_2nd_color} WHERE lace_id = ${id}`;
      } else if (on_shoe) {
        statement = SQL`UPDATE contacts SET on_shoe=${on_shoe} WHERE lace_id = ${id}`;
      }
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the lace ${id}: ` + e.message);
    }
  }


  const getLace = async (lace_id) => {
    try{ 
    const lacesList = await db.all(SQL`SELECT lace_id AS id, lace_color, lace_2nd_color, on_shoe FROM laces WHERE lace_id = ${lace_id}`);
    const lace = lacesList[0]
    if(!lace){
      throw new Error(`lace ${id} not found`)
    }
    return lace
  }catch(e){
    throw new Error(`couldn't get the lace ${id}: `+e.message)
    }
  }


  /**
   * retrieves the contacts from the database
   */

  const getLacesList = async (orderBy) => {
    try { 
    let statement = `SELECT lace_id AS id, lace_color, lace_2nd_color, on_shoe FROM laces`
    switch(orderBy){
      case 'lace_color': statement+= ` ORDER BY lace_color`; break;
      case 'lace_2nd_color': statement+= ` ORDER BY lace_2nd_color`; break;
      case 'on_shoe': statement+= ` ORDER BY on_shoe`; break;
      default: break;
    }
    const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve laces: `+e.message)
    }
  }
  
  
  const controller = {
    createLace,
    deleteLace,
    updateLace,
    getLace,
    getLacesList
  }

  return controller

}

export default initializeDatabase
